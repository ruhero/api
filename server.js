//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyparser = require('body-parser')
app.use(bodyparser.json())

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

var movimientosJSON = require('./movimientosv2.json');

app.get('/', function(req, res) {
//  res.send("Hola mundo nodejs");
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/', function(req, res) {
    res.send('Hemos recibido su petición POST');
});

app.put('/', function(req, res) {
    res.send('Hemos recibido su petición PUT');

});

app.delete('/', function(req, res) {
    res.send('Hemos recibido su petición Delete');

});

app.get('/Clientes', function(req, res) {
  res.send("Aquí tiene a los clientes");
});

//accedemos a los datos con el req, aun no hay acceso a la base de datos
app.get('/Clientes/:idcliente', function(req, res) {
  res.send("Aquí tiene al cliente : " + req.params.idcliente);
});

app.get('/v1/Movimientos', function(req, res) {
  res.sendfile('movimientosv1.json');
});

app.get('/v2/Movimientos', function(req, res) {
   res.sendfile('movimientosv2.json');
});

app.get('/v2/Movimientos:index/:otroparametro', function(req, res) {
  console.log(req.params.index);
  console.log(req.params.otroparametro);
  res.send(movimientosJSON[req.params.index-1]);
});

app.get('/v2/movimientosq', function(req, res) {
  console.log(req.query);
  res.send("Recibido verion cambiada :" + req.query);
  //res.sendfile('movimientosv2.json');
});

app.get('/v2/Movimientos/:id/:nombre', function(req, res) {
 console.log(req.params);
 console.log(req.params.id);
 console.log(req.params.nombre);
 res.send("Recibido");
})
